#include <stdio.h>

int
main(int argc, char* argv[]){
    char key[] = {0x37};
    int key_len = sizeof(key);
    char msg[] = {0x76};	
    for(int i = 0; i < sizeof(msg); i++){
	    printf("%c", msg[i] ^ key[i % key_len]);
    }
    return 0;
}
